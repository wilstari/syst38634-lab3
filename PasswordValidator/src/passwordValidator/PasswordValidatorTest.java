package passwordValidator;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void checkPasswordLengthHappy() {
		boolean pass=PasswordValidator.checkPasswordLength("password123");
		assertTrue("The password isn't suitable",pass==true);
	}
	@Test
	public void checkPasswordLengthException() {
		boolean pass=PasswordValidator.checkPasswordLength("p");
		assertTrue("The password isn't suitable",pass==false);
	}
	@Test
	public void checkPasswordLengthBoundaryIn() {
		boolean pass=PasswordValidator.checkPasswordLength("Passwo12");
		assertTrue("The password isn't suitable",pass==true);
	}
	@Test
	public void checkPasswordLengthBoundaryOut() {
		boolean pass=PasswordValidator.checkPasswordLength("Passwo1");
		assertTrue("The password isn't suitable",pass==false);
	}
	

	@Test
	public void checkDigitsHappy() {
		boolean pass=PasswordValidator.checkDigits("password123");
		assertTrue("The password isn't suitable",pass==true);
	}
	@Test
	public void checkDigitsException() {
		boolean pass=PasswordValidator.checkDigits("p");
		assertTrue("The password isn't suitable",pass==false);
	}
	@Test
	public void checkDigitsBoundaryIn() {
		boolean pass=PasswordValidator.checkDigits("passwo12");
		assertTrue("The password isn't suitable",pass==true);
	}
	@Test
	public void checkDigitsBoundaryOut() {
		boolean pass=PasswordValidator.checkDigits("passwo1");
		assertTrue("The password isn't suitable",pass==false);
	}


}
